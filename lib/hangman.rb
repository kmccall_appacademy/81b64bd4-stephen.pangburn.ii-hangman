class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    dict = File.readlines(File.dirname(__FILE__) + "/dictionary.txt")
    defaults = {
      guesser: ComputerPlayer.new(dict),
      referee: HumanPlayer.new
    }
    options = defaults.merge(options)

    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def setup
    @board = Array.new(@referee.pick_secret_word, "_")
    @guesser.register_secret_length(@board.length)
  end

  def play
    puts "\nHangman v.0.0.1\n-------------------\n"
    setup
    # p @referee.secret_word  # Debugging
    loop do
      display
      take_turn
      if @board.none? { |letter| letter == "_" }
        display
        puts "Game Over!\n\n"
        break
      end
    end
  end

  def display
    puts "\n#{@board.join(' ')}\n\n"
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(guess, indices)
    @guesser.handle_response(guess, indices)
  end

  def update_board(guess, indices)
    unless indices.empty?
      indices.each do |index|
        @board[index] = guess
      end
    end
  end
end

class HumanPlayer
  attr_reader :secret_word

  def initialize; end

  # Guesser

  def guess(_board)
    input = ""
    print "Please enter your guess: "

    loop do
      input = gets.chomp
      if input.length > 1
        print "Guesses must be a single character: "
      else
        break
      end
    end
    input
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def handle_response(_guess, indices)
    puts
    if indices.empty?
      puts "No matches."
    elsif indices.length == 1
      puts "Match at posiion #{indices[0]}."
    elsif indices.length == 2
      puts "Matches at positions #{indices[0]} and #{indices[1]}."
    else
      puts "Matches at positions #{indices[0...-1].join(', ')}, and #{indices[-1]}."
    end
    puts
  end

  # Referee

  def pick_secret_word
    input = ""
    print "How long is the word you are thinking of? "

    loop do
      input = gets.chomp

      if !(1..9).include?(input.to_i)
        print "Length must be between 1 and 9: "
      else
        break
      end
    end
    input.to_i
  end

  def check_guess(letter)
    print "My guess is '#{letter}'. Did I get anything? "
    input = gets.chomp.gsub(/\D/, "")
    input.split("").map(&:to_i)
  end
end

class ComputerPlayer
  attr_reader :secret_word
  def initialize(dict)
    @dictionary = dict
    @candidates = @dictionary
  end

  # Referee

  def pick_secret_word
    @secret_word = @dictionary.sample.chomp
    @secret_word.length
  end

  def check_guess(letter)
    indices = []

    @secret_word.chars.each_with_index do |char, i|
      indices << i if char == letter
    end
    indices
  end

  # Guesser

  def guess(board)
    letter_counts = Hash.new(0)

    @candidates.each do |word|
      word.chomp.chars.each do |char|
        letter_counts[char] += 1 unless board.include?(char)
      end
    end

    letter_counts.sort_by { |_k, v| v }[-1][0]
  end

  def register_secret_length(length)
    @secret_length = length
    @candidates.select! { |word| word.chomp.length == @secret_length }
  end

  def handle_response(guess, indices)
    if indices.empty?
      # Wrong letter
      @candidates.reject! do |candidate|
        candidate.include?(guess)
      end
    else
      # Correct letter
      @candidates.select! do |candidate|
        indices.all? do |index|
          candidate[index] == guess
        end
      end
    end

    @candidates.reject! do |candidate|
      # Correct letter, wrong placement
      match = false
      candidate.chars.each_with_index do |letter, i|
        next if indices.include?(i)
        match = true if letter == guess
      end
      match
    end
  end

  def candidate_words
    @candidates
  end
end

if __FILE__ == $PROGRAM_NAME
  Hangman.new.play
end
